FROM golang:alpine AS build-env
RUN mkdir /go/src/app && apk update && apk add git
WORKDIR /go/src/app
COPY main.go /go/src/app
RUN go mod init main.go
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o app .
FROM scratch
WORKDIR /app
COPY --from=build-env /go/src/app/app .
ENTRYPOINT [ "./app" ]
